// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

class Strings {
  static const appId = "com.gitlab.kdeleteme.hectic";
  static const appName = "Hectic";
  static const appDescription = "Organize your day";
  static const appAuthor = "Kent Delante";
  static const appAuthorDisplay = "Created by $appAuthor";
  static const appVersion = "0.5.0";
  static const appVersionLabel = "version";
  static const dbName = "Hectic.db";

  static const appAbout = "About";
  static const appSettings = "Settings";

  static const plan = "Plan";
  static const makePlan = "Make a Plan";
  static const planSaved = "Plan Saved";
  static const planUpdated = "Plan Updated";
  static const planDeleted = "Plan Deleted";
  static const noPlansShown = "There are no plans to show";
  static const editPlan = "Edit Plan";
  static const editButton = "Edit";
  static const notes = "Notes";
  static const scheduleFor = "Schedule for";
  static const custom = "Custom";

  static const defaultTime = "00:00";
  static const doneEditing = "Done Editing";
  static const startTime = "Start Time";
  static const endTime = "End Time";
  static const setReminder = "Set Reminder";
  static const planIsRequired = "Adding a plan is required";

  static const planSchedulerId = "PLAN_REMINDER";
  static const reminderForPlans = "Reminders for Plans";
  static const planSetForToday = "You have set a plan for today";
  static const remindMe = "Remind Me";
  static const minutesBefore = "minutes before";
  static const hourBefore = "hour before";

  static const licensedUnderGplThree = "Licensed under the Gnu General "
      "Public License version 3.0 (see https://www.gnu.org/licenses/gpl-3.0."
      "html)";
}
