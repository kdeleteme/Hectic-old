// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import "dart:async";
import "dart:typed_data";

import "package:flutter_local_notifications/flutter_local_notifications.dart";

import "strings.dart";

class PlanReminder {
  static final FlutterLocalNotificationsPlugin notificationsPlugin =
      FlutterLocalNotificationsPlugin();
  static final AndroidInitializationSettings androidInitializationSettings =
      AndroidInitializationSettings("app_icon");
  static final IOSInitializationSettings iosInitializationSettings =
      IOSInitializationSettings();

  PlanReminder() {
    InitializationSettings initializationSettings = InitializationSettings(
        androidInitializationSettings, iosInitializationSettings);
    notificationsPlugin.initialize(initializationSettings);
  }

  Future schedule(
      int id, String description, DateTime notificationSchedule) async {
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
      Strings.planSchedulerId,
      Strings.plan,
      Strings.reminderForPlans,
      importance: Importance.Max,
      priority: Priority.Max,
      enableVibration: true,
      vibrationPattern: Int64List.fromList([0, 1000, 5000, 2000]),
    );
    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();
    NotificationDetails notificationDetails =
        NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await notificationsPlugin.schedule(id, Strings.planSetForToday, description,
        notificationSchedule, notificationDetails);
  }

  Future cancel(int id) async {
    await notificationsPlugin.cancel(id);
  }
}
