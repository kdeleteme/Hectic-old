# Hectic

<img alt="Hectic Logo" src="assets/icons/hectic.png" width="120" />

Create and manage your plans from day to day.

<script src="https://liberapay.com/kdeleteme/widgets/button.js"></script>
<noscript>
    <a href="https://liberapay.com/kdeleteme/donate">
        <img alt="Donate using Liberapay" 
        src="https://liberapay.com/assets/widgets/donate.svg">
    </a>
</noscript>


## Features

* Add plans tagged with specific time frames.
* Each plan may contain notes, adding further details to them.
* Jump to a specific date to view past or future plans.
* Set specific dates to each plan.
* Set reminders to plans (Currently reminds a user five minutes before the 
  plan).

## Roadmap

Hectic is currently in a working state but much needs to be done.

### Features to Add (in no specific order)
- [X] Set specific time to reminders.
- [ ] Switch between light and dark themes.
- [ ] Goal Setting.
- [ ] Android home screen widget

### Features to consider (in no specific order)
- [ ] Set priorities to reminders.
- [ ] Text formatting.
- [ ] Allow users to customize font size.
- [ ] Google Calendar/{other calendar solution here} integration (?)

## Copyright

![GPL-3.0](assets/img/gplv3.png)

Copyright (C) 2018  Kent Delante

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## This program is built with Flutter

Flutter is a new way to build mobile apps, view their online
[documentation](https://flutter.io/).
