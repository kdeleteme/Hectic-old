package com.gitlab.kdeleteme.hectic

import android.os.Bundle
import android.text.format.DateFormat

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : FlutterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        MethodChannel(flutterView, HOUR_FORMAT_CHANNEL).setMethodCallHandler {
            call, result ->
            val is24HourFormat = DateFormat.is24HourFormat(this)
            if (call.method == "getIf24HourFormat") {
                result.success(is24HourFormat)
            } else {
                result.notImplemented()
            }
        }
    }

    companion object {
        private const val HOUR_FORMAT_CHANNEL = "com.gitlab.kdeleteme.hectic/hourformat"
    }
}
