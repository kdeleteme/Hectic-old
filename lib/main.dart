// Copyright (C) 2018  Kent Delante

// This file is part of Hectic
// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

import 'ui/about.dart';
import 'ui/home.dart';
import 'ui/settings.dart';
import 'utils/strings.dart';
import 'utils/theme.dart';

void main() => runApp(Hectic());

class Hectic extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Strings.appName,
      theme: ThemeData(
        primaryColor: HecticTheme.primaryColor,
        accentColor: HecticTheme.accentColor,
      ),
      home: Home(title: Strings.appName),
      routes: {
        '/settings': (context) => Settings(title: Strings.appSettings),
        '/about': (context) => About(title: Strings.appAbout),
      },
    );
  }
}
