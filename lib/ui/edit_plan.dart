// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show DateFormat;

import '../persistence/plan.dart';
import '../utils/edit_utils.dart';
import '../utils/reminder_utils.dart';
import '../utils/strings.dart';

class EditPlan extends StatefulWidget {
  EditPlan({
    Key key,
    this.title,
    this.plan,
    this.todayDate,
    this.jumpDate,
    this.editMode,
  }) : super(key: key);

  final String title;
  final Plan plan;
  final EditUtils editMode;
  final int jumpDate;
  final int todayDate;

  @override
  _EditPlanState createState() => _EditPlanState();
}

class _EditPlanState extends State<EditPlan> {
  var _startTime = 0;
  var _endTime = 0;
  var _startTimeString = Strings.defaultTime;
  var _endTimeString = Strings.defaultTime;
  var _reminderUtil = ReminderUtils.fiveMinutes;
  int _reminderSchedule;
  int _scheduleDate;
  int _jumpDate;
  Plan _plan;

  GlobalKey<FormState> _editPlanFormKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _planDetailsController = TextEditingController();
  final TextEditingController _planNotesController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _plan = Plan.clone(widget.plan);
    _scheduleDate = widget.jumpDate;
    _jumpDate = widget.jumpDate;
    _reminderSchedule = DateTime.now().millisecondsSinceEpoch;

    if (widget.editMode == EditUtils.editData) {
      _planDetailsController.text = _plan.details;
      _planNotesController.text = _plan.notes;
      _startTime = _plan.startTime;
      _endTime = _plan.endTime;
      _scheduleDate = _plan.dateScheduled;
      _setTimeFrames(_plan.startTime, _plan.endTime);
    }
  }

  void _setTimeFrames(int startTime, int endTime) {
    DateFormat dateFormatter = DateFormat.jm();
    String startTimeString =
        dateFormatter.format(DateTime.fromMillisecondsSinceEpoch(startTime));
    String endTimeString =
        dateFormatter.format(DateTime.fromMillisecondsSinceEpoch(endTime));

    setState(() {
      _startTimeString = startTimeString;
      _endTimeString = endTimeString;
    });
  }

  Future<TimeOfDay> _pickTime(BuildContext context) async {
    return await showTimePicker(context: context, initialTime: TimeOfDay.now());
  }

  Future<Null> _pickScheduleDate(BuildContext context) async {
    final DateTime initialDate = DateTime.fromMillisecondsSinceEpoch(_jumpDate);

    final int fiveYears = 1825;

    final DateTime pickedDate = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime.fromMillisecondsSinceEpoch(widget.todayDate),
      lastDate: initialDate.add(Duration(days: fiveYears)),
    );

    if (pickedDate != null) {
      final DateTime startTime =
          DateTime.fromMillisecondsSinceEpoch(_startTime);
      final DateTime endTime = DateTime.fromMillisecondsSinceEpoch(_endTime);

      final int newStartTime = DateTime(
        pickedDate.year,
        pickedDate.month,
        pickedDate.day,
        startTime.hour,
        startTime.minute,
      ).millisecondsSinceEpoch;

      final int newEndTime = DateTime(
        pickedDate.year,
        pickedDate.month,
        pickedDate.day,
        endTime.hour,
        endTime.minute,
      ).millisecondsSinceEpoch;

      setState(() {
        _scheduleDate = pickedDate.millisecondsSinceEpoch;
        _startTime = newStartTime;
        _endTime = newEndTime;
      });
    }
  }

  Future<Null> _pickStartTime(BuildContext context) async {
    final TimeOfDay picked = await _pickTime(context);

    if (picked != null && picked.toString() != _startTimeString) {
      final String hour =
          picked.hour < 10 ? '0${picked.hour}' : '${picked.hour}';
      final String minute =
          picked.minute < 10 ? '0${picked.minute}' : '${picked.minute}';

      DateTime schedule = DateTime.fromMillisecondsSinceEpoch(_scheduleDate);

      setState(() {
        _startTimeString = '$hour:$minute';
        _startTime = DateTime(schedule.year, schedule.month, schedule.day,
                picked.hour, picked.minute)
            .millisecondsSinceEpoch;
      });
    }
  }

  Future<Null> _pickEndTime(BuildContext context) async {
    TimeOfDay picked = await _pickTime(context);
    if (picked != null && picked.toString() != _endTimeString) {
      String hour = picked.hour < 10 ? '0${picked.hour}' : '${picked.hour}';
      String minute =
          picked.minute < 10 ? '0${picked.minute}' : '${picked.minute}';

      DateTime schedule = DateTime.fromMillisecondsSinceEpoch(_scheduleDate);

      setState(() {
        _endTimeString = '$hour:$minute';
        _endTime = DateTime(schedule.year, schedule.month, schedule.day,
                picked.hour, picked.minute)
            .millisecondsSinceEpoch;
      });
    }
  }

  void _saveData(BuildContext context) {
    if (_editPlanFormKey.currentState.validate()) {
      _plan.details = _planDetailsController.text;
      _plan.notes = _planNotesController.text;
      _plan.startTime = _startTime;
      _plan.endTime = _endTime;
      _plan.dateScheduled = _scheduleDate;

      if (_plan.isReminderSet) {
        _plan.reminderSchedule = _reminderSchedule;
      }

      Navigator.pop(context, _plan.toMap());
    }
  }

  String _displayScheduleDate(BuildContext context) {
    DateFormat formatter = DateFormat.yMMMd();
    return formatter.format(DateTime.fromMillisecondsSinceEpoch(_scheduleDate));
  }

  Future _scheduleReminder(BuildContext context) async {
    var jumpDate = DateTime.fromMillisecondsSinceEpoch(_jumpDate);
    var startTime = DateTime.fromMillisecondsSinceEpoch(_startTime);

    switch (_reminderUtil) {
      case ReminderUtils.fiveMinutes:
        _reminderSchedule =
            startTime.subtract(Duration(minutes: 5)).millisecondsSinceEpoch;
        break;
      case ReminderUtils.tenMinutes:
        _reminderSchedule =
            startTime.subtract(Duration(minutes: 10)).millisecondsSinceEpoch;
        break;
      case ReminderUtils.twentyMinutes:
        _reminderSchedule =
            startTime.subtract(Duration(minutes: 20)).millisecondsSinceEpoch;
        break;
      case ReminderUtils.thirtyMinutes:
        _reminderSchedule =
            startTime.subtract(Duration(minutes: 30)).millisecondsSinceEpoch;
        break;
      case ReminderUtils.oneHour:
        _reminderSchedule =
            startTime.subtract(Duration(hours: 1)).millisecondsSinceEpoch;
        break;
      default:
        var pickedTime = await showTimePicker(
          initialTime: TimeOfDay
              .fromDateTime(DateTime.fromMillisecondsSinceEpoch(_startTime)),
          context: context,
        );

        if (pickedTime != null) {
          _reminderSchedule = DateTime(jumpDate.year, jumpDate.month,
                  jumpDate.day, pickedTime.hour, pickedTime.minute)
              .millisecondsSinceEpoch;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              _saveData(context);
            },
            tooltip: Strings.doneEditing,
            icon: Icon(Icons.done),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: Form(
          key: _editPlanFormKey,
          child: ListView(
            children: <Widget>[
              TextFormField(
                controller: _planDetailsController,
                style: TextStyle(
                  fontSize: 18.0,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  labelText: Strings.plan,
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return Strings.planIsRequired;
                  }
                },
              ),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
                child: Text(
                  Strings.scheduleFor,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 16.0),
                child: FlatButton(
                  onPressed: () => _pickScheduleDate(context),
                  child: Text(
                    _displayScheduleDate(context),
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      child: Column(
                        children: <Widget>[
                          Text(
                            Strings.startTime,
                            style: TextStyle(fontSize: 18.0),
                          ),
                          FlatButton(
                            onPressed: () {
                              _pickStartTime(context);
                            },
                            child: Text(
                              _startTimeString,
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Column(
                        children: <Widget>[
                          Text(
                            Strings.endTime,
                            style: TextStyle(fontSize: 16.0),
                          ),
                          FlatButton(
                            onPressed: () {
                              _pickEndTime(context);
                            },
                            child: Text(
                              _endTimeString,
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SwitchListTile(
                title: Text(
                  Strings.setReminder,
                  style: TextStyle(fontSize: 18.0),
                ),
                value: _plan.isReminderSet,
                onChanged: (value) {
                  setState(() {
                    _plan.isReminderSet = value;
                  });
                },
              ),
              Text(
                Strings.remindMe,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18.0,
                  color: _plan.isReminderSet ? Colors.black : Colors.grey,
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: DropdownButton(
                  value: _plan.isReminderSet ? _reminderUtil : null,
                  onChanged: _plan.isReminderSet
                      ? (value) {
                          setState(() {
                            _reminderUtil = value;
                          });

                          _scheduleReminder(context);
                        }
                      : (value) {},
                  items: [
                    DropdownMenuItem(
                      child: Text('5 ${Strings.minutesBefore}'),
                      value: ReminderUtils.fiveMinutes,
                    ),
                    DropdownMenuItem(
                      child: Text('10 ${Strings.minutesBefore}'),
                      value: ReminderUtils.tenMinutes,
                    ),
                    DropdownMenuItem(
                      child: Text('15 ${Strings.minutesBefore}'),
                      value: ReminderUtils.fifteenMinutes,
                    ),
                    DropdownMenuItem(
                      child: Text('20 ${Strings.minutesBefore}'),
                      value: ReminderUtils.twentyMinutes,
                    ),
                    DropdownMenuItem(
                      child: Text('30 ${Strings.minutesBefore}'),
                      value: ReminderUtils.thirtyMinutes,
                    ),
                    DropdownMenuItem(
                      child: Text('1 ${Strings.hourBefore}'),
                      value: ReminderUtils.oneHour,
                    ),
                    DropdownMenuItem(
                      child: Text(Strings.custom),
                      value: ReminderUtils.custom,
                    ),
                  ],
                ),
              ),
              TextFormField(
                maxLines: null,
                keyboardType: TextInputType.multiline,
                controller: _planNotesController,
                style: TextStyle(
                  fontSize: 18.0,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  labelText: Strings.notes,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
