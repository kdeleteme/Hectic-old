// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show DateFormat;

import '../persistence/plan.dart';
import '../utils/edit_utils.dart';
import '../utils/strings.dart';
import 'edit_plan.dart';

class Planner extends StatelessWidget {
  Planner({
    Key key,
    this.plans,
    this.todayDate,
    this.jumpDate,
    this.onDelete,
    this.onChange,
    this.cancelReminder,
    this.setReminder,
    this.is24HourFormat,
  }) : super(key: key);

  final List<Plan> plans;
  final int todayDate;
  final int jumpDate;
  final ValueChanged<int> onDelete;
  final ValueChanged<Plan> onChange;
  final ValueChanged<int> cancelReminder;
  final ValueChanged<Plan> setReminder;
  final bool is24HourFormat;

  void _handleDelete(int id) {
    onDelete(id);
  }

  void _handleUpdate(Plan plan) {
    onChange(plan);
  }

  Future _editPlan(BuildContext context, Plan plan) async {
    final Map result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditPlan(
              title: Strings.editPlan,
              editMode: EditUtils.editData,
              plan: plan,
              todayDate: todayDate,
              jumpDate: jumpDate,
            ),
      ),
    );

    if (result != null) {
      final newPlan = Plan.fromMap(result);

      if (plan.hasChanged(newPlan)) {
        _handleUpdate(newPlan);

        if (plan.isReminderSet && !newPlan.isReminderSet) {
          cancelReminder(newPlan.id);
        }

        if (!plan.isReminderSet && newPlan.isReminderSet) {
          setReminder(newPlan);
        }
      }
    }
  }

  String _displayTimeFrame(int startTime, int endTime) {
    DateFormat dateFormatter = DateFormat.jm();

    if (is24HourFormat) dateFormatter = DateFormat.Hm();

    final String startTimeString =
        dateFormatter.format(DateTime.fromMillisecondsSinceEpoch(startTime));
    final String endTimeString =
        dateFormatter.format(DateTime.fromMillisecondsSinceEpoch(endTime));
    return '$startTimeString - $endTimeString';
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: plans.length,
      itemBuilder: (context, index) {
        final Plan plan = plans[index];
        return Dismissible(
          key: Key(plan.id.toString()),
          background: Container(
            color: Colors.red,
            alignment: AlignmentDirectional.centerEnd,
            child: Padding(
              padding: EdgeInsets.only(
                right: 16.0,
              ),
              child: Icon(
                Icons.delete,
                color: Colors.white,
              ),
            ),
          ),
          direction: DismissDirection.endToStart,
          onDismissed: (direction) {
            _handleDelete(plan.id);
            plans.removeAt(index);
          },
          child: GestureDetector(
            onLongPress: () {
              _editPlan(context, plan);
            },
            child: ExpansionTile(
              leading: Icon(
                Icons.assignment,
                color: plan.isReminderSet ? Colors.orangeAccent : Colors.grey,
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    plan.details,
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.black87,
                    ),
                  ),
                  Text(
                    _displayTimeFrame(
                      plan.startTime,
                      plan.endTime,
                    ),
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 16.0,
                    ),
                  ),
                ],
              ),
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(16.0),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    (plan.notes == null || plan.notes.isEmpty)
                        ? 'No notes to show'
                        : plan.notes,
                    style: TextStyle(
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
